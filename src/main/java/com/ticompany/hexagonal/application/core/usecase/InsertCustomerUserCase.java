package com.ticompany.hexagonal.application.core.usecase;

import com.ticompany.hexagonal.application.core.domain.Customer;
import com.ticompany.hexagonal.application.ports.out.FindAddressByZipcodeOutPutPort;
import com.ticompany.hexagonal.application.ports.out.InsertCustomerOutputPort;

public class InsertCustomerUserCase {

    private final FindAddressByZipcodeOutPutPort findAddressByZipcodeOutPutPort;
    private final InsertCustomerOutputPort insertCustomerOutputPort;
    
    public InsertCustomerUserCase(FindAddressByZipcodeOutPutPort findAddressByZipcodeOutPutPort, InsertCustomerOutputPort insertCustomerOutputPort){
        this.findAddressByZipcodeOutPutPort = findAddressByZipcodeOutPutPort;
        this.insertCustomerOutputPort = insertCustomerOutputPort;
    }

    public void insert(Customer customer, String zipCode){
        var address = this.findAddressByZipcodeOutPutPort.find(zipCode);
        customer.setAddress(address);
        this.insertCustomerOutputPort.insert(customer);
    }
}
