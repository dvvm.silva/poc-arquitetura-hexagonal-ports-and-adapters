package com.ticompany.hexagonal.application.ports.out;

import com.ticompany.hexagonal.application.core.domain.Address;

public interface FindAddressByZipcodeOutPutPort {
    Address find(String zipCode);
}
