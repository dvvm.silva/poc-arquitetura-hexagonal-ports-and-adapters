package com.ticompany.hexagonal.application.ports.out;

import com.ticompany.hexagonal.application.core.domain.Customer;

public interface InsertCustomerOutputPort {
	void insert(Customer customer);
}
