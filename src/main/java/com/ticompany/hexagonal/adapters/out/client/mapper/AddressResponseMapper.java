package com.ticompany.hexagonal.adapters.out.client.mapper;

import org.mapstruct.Mapper;

import com.ticompany.hexagonal.adapters.out.client.response.AddressResponse;
import com.ticompany.hexagonal.application.core.domain.Address;

@Mapper(componentModel = "string")
public interface AddressResponseMapper {
	Address toAddress(AddressResponse addressResponse);
	
}
