package com.ticompany.hexagonal.adapters.out;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ticompany.hexagonal.adapters.out.client.FindAddressByZipCodeClient;
import com.ticompany.hexagonal.adapters.out.client.mapper.AddressResponseMapper;
import com.ticompany.hexagonal.application.core.domain.Address;
import com.ticompany.hexagonal.application.ports.out.FindAddressByZipcodeOutPutPort;

@Component
public class FindAddressByZipCodeAdapter implements FindAddressByZipcodeOutPutPort{

	@Autowired
	private FindAddressByZipCodeClient findAddressByZipCodeClient;
	
	@Autowired
	private AddressResponseMapper addressResponseMapper;
	
	@Override
	public Address find(String zipCode) {
		return this.addressResponseMapper.toAddress(this.findAddressByZipCodeClient.find(zipCode));
	}

	
}
	